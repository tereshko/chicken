#include "function.h"
#include "coloredchickencell.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

int openFile(){
	int fd;
	fd = open( "binTereshko", O_RDWR);
	if (-1 == fd) {
		fd = createFile();
	}
	return fd;
}

int createFile() {
	int fd;
	std::cout << "\nFile not found. We will create new file.\n";
	fd = open( "binTereshko", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
	return fd;
}

void saveData( ChickenCell* coloredChickenCell, int fd) {
	if (fd >= 0) {
		coloredChickenCell->saveData(fd);
	}
}

void closeDataFile( int fd){
	if (fd >= 0) {
		close( fd);
	}
}

void loadData( ColoredChickenCell* coloredChickenCell, int fd){
	if (fd >= 0) {
		coloredChickenCell->loadData( fd);
	}
}

void displayAdditionalInformation( int collectedEggs, int stepCount) {
	std::cout << "\n---------";
	std::cout << "Step: " << stepCount;
	std::cout << "-----";
	std::cout << "---------\n";
	std::cout << "Total Eggs in the basket: " << collectedEggs << "\n";
	std::cout << "------------------------------\n\n";
}

