#ifndef H_ACTION
#define H_ACTION
enum Action
{
	ACTION__DO_NOTHING,
	ACTION__ADD_SEEDS,
	ACTION__TAKE_EGG,
	ACTION__SET_COLOR,
};

enum MainMenuAction
{
	MAINMENUACTION__NEW_GAME,
	MAINMENUACTION__CONTINUE,
	MAINMENUACTION__SAVE,
	MAINMENUACTION__LOAD,
	MAINMENUACTION__EXIT

};
#endif
