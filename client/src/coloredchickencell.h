#include "chickencell.h"

#ifndef H_COLOREDCHICKENCELL
#define H_COLOREDCHICKENCELL
class ColoredChickenCell : public ChickenCell {

public:
	enum Color {
		BLACK,
		ORANGE,
		GREEN
	};

	void applyAction( int* collectedEggs, Action choiseAction);
	ColoredChickenCell();
	void setColor();
	void showColor( int colorOfChicken);

	void display() override;
	void saveData(int fd) override;
	void loadData(int fd) override;
private:
	Color m_color;
};
#endif
