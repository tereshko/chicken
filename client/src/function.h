#include "chickencell.h"
#include "coloredchickencell.h"

#ifndef H_FUNCTION
#define H_FUNCTION

void displayAdditionalInformation( int collectedEggs, int stepCount);
int openFile();
int createFile();
void saveData(ChickenCell* coloredChickenCell, int fd);
void loadData( ColoredChickenCell* coloredChickenCell, int fd);
void closeDataFile( int fd);
#endif

#define LOG(paramOne, paramSec) std::cout << "Debug." << paramOne << " " << paramSec << "\n"
