#include "coloredchickencell.h"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

ColoredChickenCell::ColoredChickenCell() {
	this->m_color = ORANGE;
}

void ColoredChickenCell::saveData( int fd)
{
	ChickenCell::saveData(fd);
	write( fd, &m_color, sizeof(int));
}

void ColoredChickenCell::loadData(int fd){

	ChickenCell::loadData( fd);

	int chickenColor;
	read( fd, &chickenColor, sizeof(int));
	this->m_color = (Color) chickenColor;
}

void ColoredChickenCell::display() {
	ChickenCell::display();
	std::cout << "COLOR: ";
	ColoredChickenCell::showColor( this->m_color);
	std::cout << "\n\n";
}

void ColoredChickenCell::applyAction( int* collectedEggs, Action choiseAction) {

	switch (choiseAction) {

	case ACTION__SET_COLOR:
		ColoredChickenCell::setColor();
		break;
	default:
		ChickenCell::applyAction( collectedEggs, choiseAction);
	}
}

void ColoredChickenCell::setColor() {

	bool goodAnswer = false;
	int selectedColor;
	while( !goodAnswer) {
		std::cout << "\nSet color: \n";
		std::cout << ColoredChickenCell::Color::BLACK << ". Black\n";
		std::cout << ColoredChickenCell::Color::ORANGE << ". Orange\n";
		std::cout << ColoredChickenCell::Color::GREEN << ". Green\n";
		std::cout << "Please select the color: ";
		std::cin >> selectedColor;

		if( (ColoredChickenCell::Color::BLACK <= selectedColor) &&  (ColoredChickenCell::Color::GREEN >= selectedColor)) {
			goodAnswer = true;
		}
	}

	m_color = (ColoredChickenCell::Color)selectedColor;
}

void ColoredChickenCell::showColor( int colorOfChicken) {
	switch( colorOfChicken) {
	case BLACK:
		std::cout << "Black";
		break;
	case ORANGE:
		std::cout << "Orange";
		break;
	case GREEN:
		std::cout << "Green";
		break;
	}
}



