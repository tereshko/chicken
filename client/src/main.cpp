#include "Game.h"
#include "action.h"
#include "function.h"

#include <iostream>

int main (int argc, char** argv) {

	Game game;

	MainMenuAction myMainMenuAction;
	bool isExitRequested = false;
	while( !isExitRequested){

		myMainMenuAction = game.getUserActionInMainMenu();
		switch( myMainMenuAction){
		case MAINMENUACTION__NEW_GAME:
			game.newGame();

		case MAINMENUACTION__CONTINUE:
			game.continueGame();
			break;

		case MAINMENUACTION__SAVE:
			game.save();
			break;

		case MAINMENUACTION__LOAD:
			game.load();
			break;

		case MAINMENUACTION__EXIT:
			isExitRequested = true;
			break;
		}
	}
	return 0;
}




