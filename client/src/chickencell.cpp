#include "chickencell.h"
#include "coloredchickencell.h"
#include "function.h"

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

ChickenCell::ChickenCell() {
	this->m_seedsCount = 5;
	this->m_isChickenAlive = true;
	this->m_isEggExist = false;
}

void ChickenCell::saveData(int fd)
{
	write( fd, &m_seedsCount, sizeof( int));

	int isEggExist = m_isEggExist ? 1 : 0;
	write( fd, &isEggExist, sizeof( int));

	int isChickenAlive = m_isChickenAlive ? 1 : 0;
	write( fd, &isChickenAlive, sizeof( int));
}

void ChickenCell::loadData( int fd) {

	int seedsCount = 0;
	read( fd, &seedsCount, sizeof(int));

	int isChickenAlive;
	read( fd, &isChickenAlive, sizeof(int));

	int isEggExist;
	read( fd, &isEggExist, sizeof(int));

	this->m_seedsCount = seedsCount;
	this->m_isChickenAlive = isChickenAlive;
	this->m_isEggExist = isEggExist;
}

void ChickenCell::display() {

	std::cout << "Status of chicken: ";
	if (this->m_isChickenAlive) {
		std::cout << "alive\n";
	} else {
		std::cout << "dead\n";
	}
	std::cout << "Seeds count: " << this->m_seedsCount << "\n";
	std::cout << "Egg in the kyratnik: " << this->m_isEggExist << "\n";
	std::cout << "------------------------------------------------\n";
}

bool ChickenCell::applyAction( int* collectedEggs, Action choiseAction) {
	bool isExitRequested = false;
	switch (choiseAction) {
	case ACTION__DO_NOTHING:
		std::cout << "Play with chicken\n";
		break;
	case ACTION__ADD_SEEDS:
		std::cout << "Add 5 seeds\n";
		this->m_seedsCount = m_seedsCount + 5;
		break;
	case ACTION__TAKE_EGG:
		if (this->m_isEggExist) {
			*collectedEggs += 1;
			std::cout << "We took egg\n";
			this->m_isEggExist = false;
		} else {
			std::cout << "Sorry, we you don't have eggs\n";
		}
		break;
	}
	return isExitRequested;
}

void ChickenCell::tick( int stepCount) {

//	LOG("isChickenAlive", this->m_isChickenAlive);
	std::cout << "tic" << "\n";

	if (this->m_isChickenAlive) {

		bool isfourthStep = (stepCount % 4) == 0;

//		LOG("isfourthStep", isfourthStep);
//		LOG("cellForChicken->m_isEggExist", this->m_isEggExist);

		if (isfourthStep && !this->m_isEggExist) {
			this->m_isEggExist = true;
		}

		if (this->m_seedsCount > 0){
			this->m_seedsCount -= 1;
		} else {
			this->m_isChickenAlive = false;
		}
	}
}
