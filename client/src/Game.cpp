#include "Game.h"
#include "coloredchickencell.h"
#include "function.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

Game::Game() {
	this->m_chickens = 2;
	this->m_collectedEggs = 0;
	this->m_stepCount = 1;
	this->m_coloredChickenCell = new ColoredChickenCell[this->m_chickens];
}

Game::~Game() {
	delete [] this->m_coloredChickenCell;
}

void Game::newGame() {
	delete [] this->m_coloredChickenCell;
	this->m_coloredChickenCell = new ColoredChickenCell[this->m_chickens];
	this->m_collectedEggs = 0;
	this->m_stepCount = 1;
}

void Game::continueGame() {

	bool isExitRequested = false;

	for (this->m_stepCount; !isExitRequested; this->m_stepCount++) {

		for (int i = 0; i < this->m_chickens; i++) {

			this->m_coloredChickenCell[i].tick( this->m_stepCount);
		}

		displayAdditionalInformation( this->m_collectedEggs, this->m_stepCount);

		for (int i = 0; i < this->m_chickens; i++) {

			std::cout << "\nChciken: " << i << "\n";
			this->m_coloredChickenCell[i].display();
		}

		int selectedChicken = getChickenNumber();
		if(selectedChicken == this->m_chickens){
			isExitRequested = true;
		} else {
			Action choiseAction;
			choiseAction = getUserAction();
			this->m_coloredChickenCell[selectedChicken].applyAction( &m_collectedEggs, choiseAction);
		}
	}
}

void Game::save() {
	int fd = openFile();
	for( int i = 0; i < this->m_chickens; i++){
		saveData( &m_coloredChickenCell[i], fd);
		std::cout << "Save data for chicken: " << i << "\n";
	}
	saveEggs( fd);
	saveStepCount( fd);
	closeDataFile( fd);
}

void Game::saveEggs( int fd){
	int collectedEggs = this->m_collectedEggs;
	write( fd, &collectedEggs, sizeof( int));
}

void Game::saveStepCount( int fd){
	int stepCount = this->m_stepCount;
	write( fd, &stepCount, sizeof( int));
}

void Game::loadEggs( int fd){
	int collectedEggs = 0;
	read( fd, &collectedEggs, sizeof(int));
	this->m_collectedEggs = collectedEggs;
}

void Game::loadStepCount( int fd){
	int stepCount = 0;
	read( fd, &stepCount, sizeof(int));
	this->m_stepCount = stepCount;
}

void Game::load() {
	int fd = openFile();
	for( int i = 0; i < this->m_chickens; i++){
		loadData( &m_coloredChickenCell[i], fd);
		std::cout << "Load data for chicken: " << i << "\n";
	}
	loadEggs( fd);
	loadStepCount( fd);
	closeDataFile( fd);
}

int Game::getChickenNumber() {

	bool isChosenChickenValid = false;
	int chisedMenu = 0;
	int exitMenu = this->m_chickens;

	while (!isChosenChickenValid){
		std::cout << "Chickens: \n";

		for (int i = 0; i < this->m_chickens; i++) {
			std::cout << i <<". Chicken " << i << "\n";
		}

		std::cout << exitMenu << ". Exit\n";

		std::cout << "Please select the chicken: ";
		std::cin >> chisedMenu;

		isChosenChickenValid = (chisedMenu >= 0) && (chisedMenu <= this->m_chickens);
	}

	return chisedMenu;
}

Action Game::getUserAction() {
	int choiseAction = 0;
	Action myAction;

	bool isActionValid = false;
	while (!isActionValid) {

		std::cout << ACTION__DO_NOTHING << ". Do nothing \n";
		std::cout << ACTION__ADD_SEEDS << ". Add 5 seeds\n";
		std::cout << ACTION__TAKE_EGG << ". Take egg\n";
		std::cout << ACTION__SET_COLOR << ". Change color\n";
		std::cout << "Take your choise: ";
		std::cin >> choiseAction;
		std::cout << "\n\n";

//		LOG("isActionValid", isActionValid);
		isActionValid = (choiseAction >= ACTION__DO_NOTHING) && (choiseAction <= ACTION__SET_COLOR );
	}

	myAction = (Action)choiseAction;
	return myAction;
}

MainMenuAction Game::getUserActionInMainMenu(){
	int choiseAction = 0;
	MainMenuAction myMainMenuAction;

	bool isActionValid = false;

	while (!isActionValid) {

		std::cout << MAINMENUACTION__NEW_GAME << ". New Game \n";
		std::cout << MAINMENUACTION__CONTINUE << ". Continue \n";
		std::cout << MAINMENUACTION__SAVE << ". Save \n";
		std::cout << MAINMENUACTION__LOAD << ". Load \n";
		std::cout << MAINMENUACTION__EXIT << ". Exit \n";
		std::cout << "Take your choise: ";
		std::cin >> choiseAction;
		std::cout << "\n\n";

		isActionValid = (choiseAction >= MAINMENUACTION__NEW_GAME) && (choiseAction <= MAINMENUACTION__EXIT );

	}
	myMainMenuAction = (MainMenuAction)choiseAction;

	return myMainMenuAction;
}


