#include "action.h"
#include "coloredchickencell.h"

#ifndef H_GAME
#define H_GAME
class Game {
public:
	Game();
	~Game();
	int getChickenNumber();
	int chickenMenu( );

	void newGame();
	void continueGame();
	void save();
	void load();
	void saveEggs( int fd);
	void loadEggs( int fd);
	void saveStepCount( int fd);
	void loadStepCount( int fd);
	Action getUserAction();
	MainMenuAction getUserActionInMainMenu();

private:
	int m_chickens;
	int m_collectedEggs;
	int m_stepCount;
	ColoredChickenCell* m_coloredChickenCell;

};
#endif