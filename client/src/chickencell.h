#include "action.h"

#ifndef H_CHICKENCELL
#define H_CHICKENCELL
class ChickenCell {
public:
	ChickenCell();

	void tick( int stepCount);
	bool applyAction( int* collectedEggs, Action choiseAction);

	virtual void display();
	virtual void saveData(int fd);
	virtual void loadData( int fd);
private:
	int m_seedsCount;
	bool m_isChickenAlive;
	bool m_isEggExist;
};
#endif

